package com.example.al.a02_05__xml;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

//public class MainActivity extends Activity implements View.OnClickListener {
public class MainActivity extends Activity {
    TextView text1;
    EditText edit1, edit2;
    Button b1, b2, b3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text1 = (TextView)findViewById(R.id.textHello);
        edit1 = (EditText)findViewById(R.id.editText1);
        edit2 = (EditText)findViewById(R.id.editText2);
        b1 = (Button)findViewById(R.id.button1);
        b2 = (Button)findViewById(R.id.button2);
        b3 = (Button)findViewById(R.id.button3);

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text1.setText("0");
            }
        });

        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int x = Integer.decode( edit1.getText().toString() );
                int y = Integer.decode( edit2.getText().toString() );
                int result = x + y;
                text1.setText( String.valueOf(result) );
            }
        });

        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int x = Integer.decode( edit1.getText().toString() );
                int y = Integer.decode( edit2.getText().toString() );
                int result = x - y;
                text1.setText( String.valueOf(result) );
            }
        });

//        b1.setOnClickListener(this);
//        b2.setOnClickListener(this);
//        b3.setOnClickListener(this);
    }

//    @Override
//    public void onClick(View v) {
//        switch (v.getId()) {
//            case R.id.button1:
//                text1.setText("0");
//                break;
//            case R.id.button2:
//                {
//                    int x = Integer.decode(edit1.getText().toString());
//                    int y = Integer.decode(edit2.getText().toString());
//                    int result = x + y;
//                    text1.setText(String.valueOf(result));
//                }
//                break;
//            case R.id.button3:
//                {
//                    int x = Integer.decode(edit1.getText().toString());
//                    int y = Integer.decode(edit2.getText().toString());
//                    int result = x - y;
//                    text1.setText(String.valueOf(result));
//                }
//                break;
//        }
//    }
}
